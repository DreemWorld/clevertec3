package vovk.kirill.clevertec3.dataBase

import androidx.lifecycle.LiveData

class ContactRepository(private val contactDao: ContactDao) {

    val readAllData: LiveData<List<Contact>> = contactDao.getAllContact()

     suspend fun addContact(contact: Contact) {
        contactDao.addContact(contact)
    }
}