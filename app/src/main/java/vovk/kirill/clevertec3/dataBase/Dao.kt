package vovk.kirill.clevertec3.dataBase

import androidx.lifecycle.LiveData
import androidx.room.*

@Entity(tableName = "contact")
data class Contact(
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    @ColumnInfo(name = "name") val name: String?,
    @ColumnInfo(name = "number") val number: String?,
    @ColumnInfo(name = "email") val email: String?
)

@Dao
interface ContactDao {



    @Query("SELECT * FROM contact")
    fun getAllContact(): LiveData<List<Contact>>

    @Query("SELECT * FROM contact WHERE id IN (:contactIds)")
    fun loadAllByIds(contactIds: IntArray): List<Contact>

    @Query("SELECT * FROM contact WHERE name LIKE (:nameQuery) LIMIT 1")
    fun findByName(nameQuery: String): Contact

    @Query("SELECT * FROM contact WHERE email LIKE (:emailQuery) LIMIT 1")
    fun findByEmail(emailQuery: String): Contact

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun addContact(contacts: Contact)

    @Delete
    fun delete(contact: Contact)
}
