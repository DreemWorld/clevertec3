package vovk.kirill.clevertec3

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.os.Bundle
import android.provider.ContactsContract
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import vovk.kirill.clevertec3.dataBase.Contact
import vovk.kirill.clevertec3.dataBase.ContactViewModel


class MainActivity : AppCompatActivity() {

    private lateinit var selectButton: Button
    private lateinit var showContactsButton: Button
    private lateinit var contactName: TextView
    private lateinit var contactEmail: TextView
    private lateinit var contactNumber: TextView
    private lateinit var mContactViewModel: ContactViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        contactName = findViewById(R.id.nameBD)
        contactEmail = findViewById(R.id.emailBD)
        contactNumber = findViewById(R.id.numberBD)

        selectButton = findViewById(R.id.select_contact)
        selectButton.setOnClickListener {
            if (!checkContactsPermissionGranted()) {
                requestContactsPermissions()
            } else openAndPickContact()
        }
        showContactsButton = findViewById(R.id.show_contact)
        showContactsButton.setOnClickListener {
            // TODO add dialog
        }
    }


    private fun checkContactsPermissionGranted(): Boolean {
        return ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.READ_CONTACTS
        ) == PackageManager.PERMISSION_GRANTED
    }

    private fun requestContactsPermissions() {
        val permission = arrayOf(android.Manifest.permission.READ_CONTACTS)
        ActivityCompat.requestPermissions(this, permission, REQUEST_CODE_PERMISSION)
    }

    private fun openAndPickContact() {
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = ContactsContract.Contacts.CONTENT_TYPE
        startActivityForResult(intent, REQUEST_CODE_PICK)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == REQUEST_CODE_PERMISSION) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                showToast("Доступ получен")
            }
        }
    }

    @SuppressLint("Range")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CODE_PICK) {

                val uri = data!!.data!!

                var cursor1: Cursor = contentResolver.query(
                    uri,
                    arrayOf(ContactsContract.Contacts.DISPLAY_NAME),
                    null,
                    null,
                    null
                )!!

                var numberContact: String? = null
                var nameContact: String? = null
                var emailContact: String? = null

                if (cursor1.moveToFirst()) {
                    nameContact = cursor1.getString(
                        cursor1.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME)
                    )

                    var cursor2: Cursor = contentResolver.query(
                        ContactsContract.Data.CONTENT_URI,
                        arrayOf(
                            ContactsContract.Data.DISPLAY_NAME,
                            ContactsContract.Contacts.Data.DATA1,
                            ContactsContract.Contacts.Data.MIMETYPE
                        ),
                        ContactsContract.Data.DISPLAY_NAME + " = ?",
                        arrayOf(nameContact),
                        null
                    )!!

                    contactName.text = nameContact
                    // TODO name

                    if (cursor2.moveToFirst()) {
                        if (ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE.equals(
                                cursor2.getString(
                                    cursor2.getColumnIndex(
                                        ContactsContract.Contacts.Data.MIMETYPE
                                    )
                                ),
                                true
                            )
                        ) {
                            numberContact = cursor2.getString(
                                cursor2.getColumnIndex(ContactsContract.Contacts.Data.DATA1)

                            )
                            contactNumber.text = numberContact

                            // TODO number
                        }

                        if (ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE.equals(
                                cursor2.getString(
                                    cursor2.getColumnIndex(
                                        ContactsContract.Contacts.Data.MIMETYPE
                                    )
                                ),
                                true
                            )
                        ) {
                            emailContact = cursor2.getString(
                                cursor2.getColumnIndex(ContactsContract.Contacts.Data.DATA1)

                            )
                            contactEmail.text = emailContact

                            // TODO email
                        }
                        mContactViewModel = ViewModelProvider(this).get(ContactViewModel::class.java)
                    }
                    cursor2.close()
                }

                cursor1.close()

                val contact = Contact(0, nameContact, numberContact, emailContact)
                mContactViewModel.addContact(contact)
                showToast("Сохранение успешно")
            }
        }

    }


    private fun showToast(text: String) {
        val toast = Toast.makeText(applicationContext, text, Toast.LENGTH_SHORT)
        toast.show()
    }

    private companion object {
        private const val REQUEST_CODE_PERMISSION = 1
        private const val REQUEST_CODE_PICK = 2
    }

}